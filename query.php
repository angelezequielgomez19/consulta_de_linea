<?php
$agencyId = $_POST["agencyId"];
$agency = 'Bd/agency.txt';
$routes = 'Bd/routes.txt';
$trips = 'Bd/trips.txt';
$agencyName = "";
$agencyUrl = "";
$routeName = array();
$flag = 0;

if(!file_exists($agency))
{
    echo '<script> alert("archivo de agencias no existe");
                    window.location = "index.html";</script>';
}
else
{
    $file = fopen($agency, 'r') or die("no puedo abrir archivo de agencias para lectura");
    while (!feof($file) && $flag == 0)
    {
        $line = fgets($file);
        $data = explode(",", $line);
        $id = $data[0];
        if($id == $agencyId)
        {
            $agencyName = $data[1];
            $agencyUrl = $data[2];
            $flag = 1;
        }
    }
    fclose($file);
    if($flag == 1)
    {
        $file = fopen($routes, 'r') or die ("no puedo abrir archivo de rutas");
        while(!feof($file) && $flag == 1)
        {
            $line = fgets($file);
            $data = explode(",", $line);
            $id = $data[1];
            if($id == $agencyId)
            {
                $routeId = $data[0];
                $flag = 0;
            }
        }
        fclose($file);
        $file = fopen($trips, 'r') or die ("no puedo abrir archivo de rutas");
        while(!feof($file))
        {
            $line = fgets($file);
            $data = explode(",", $line);
            $id = $data[1];
            if($id == $routeId)
            {
                $flag ++;
                array_push($routeName, $data[3]);
                //$routeName = $data[3];
            }
            elseif ($id != $routeId && $flag != 0)
            {
                break;
            }
        }
        fclose($file);
        //echo $agencyName . "<br>" . $agencyUrl . "<br>" . $routeName;

        function show($array, $entero)
        {
            // Verificar si el entero es mayor a 0 y el array no está vacío
            if ($entero > 0 && !empty($array))
            {
                // Recorrer el array la cantidad de veces especificada por el entero
                for ($i = 0; $i < $entero; $i++)
                {
                    // Mostrar el elemento en la posición $i del array
                    echo $array[$i] . "<br>" ;
                }
            } 
            else 
            {
                echo "Error: El entero debe ser mayor que 0 y el array no debe estar vacío.";
            }
        }

        $contenido = '
        <!DOCTYPE html>
        <html>
            <head>
            <link rel="stylesheet" type="text/css" href="styles/styles.css">
            <meta charset="UTF-8">
            </head>
        <body>
            <div class="agency-info">
                <p>'. $agencyName .'</p>
                <p>'. $agencyUrl .'</p>
                <p>'. show($routeName, $flag).'</p>
                <a href="index.html"><button id="button">Volver a consultar</button></a>
        </div>
        </body>
        </html>
        ';
        echo $contenido;
    }
    else
    {
        echo '<script>alert("No se ha encontrado una linea con ese numero");
        window.location = "index.html";</script>';
    }
}
?>